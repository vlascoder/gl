#!/usr/bin/perl
use strict;
use warnings;

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

# Для всех коннектов требуется использовать какой-нибудь HTTPS-proxy
$ENV{HTTPS_PROXY} = 'https://68.183.80.218:3128';

# В зависимости от версий модулей LWP, Crypt-SSLeay и IO::Socket::SSL
# может отличаться порядок их загрузки и модуль по-умолчанию.
#use Net::HTTPS;
#$Net::HTTPS::SSL_SOCKET_CLASS = 'Net::SSL';

use Net::SSL;
use IO::Socket::SSL;
# $IO::Socket::SSL::DEBUG = 3;
# $ENV{'HTTPS_DEBUG'} = 1;

my $net_ssl_connect = \&Net::SSL::connect;
*Net::SSL::connect = sub
{
    require IO::Socket;

    @Net::SSL::ISA = qw(IO::Socket::INET);

    return $net_ssl_connect->(@_);
};

sub reImportHTTPS
{
    my ($class) = @_;

    map { delete $INC{ $_ } } qw(Net/HTTPS.pm);

    $Net::HTTPS::SSL_SOCKET_CLASS = $class;

    require Net::HTTPS;
    Net::HTTPS->import();
}

use TestModule1;
use TestModule2;
use TestModule3;

my $test_module1_connect = \&TestModule1::connect;
*TestModule1::connect = sub
{
    reImportHTTPS('Net::SSL');

    return $test_module1_connect->(@_);
};

my $test_module2_connect = \&TestModule2::connect;
*TestModule2::connect = sub
{
    reImportHTTPS('IO::Socket::SSL');

    return $test_module2_connect->(@_);
};

my $test_module3_connect = \&TestModule3::connect;
*TestModule3::connect = sub
{
    reImportHTTPS('Net::SSL');

    return $test_module3_connect->(@_);
};

# Сервер поддерживает старые SSL-протоколы, исторически используется Net::SSL
print TestModule1->connect('https://api.ipify.org/');

# Сервер поддерживает только TLS 1.2, требуется использовать IO::Socket::SSL
print TestModule2->connect('https://fancyssl.hboeck.de/');

# Сервер поддерживает старые SSL-протоколы, требуется использовать Net::SSL
print TestModule3->connect('https://api.ipify.org/');

print "\nDone\n";

