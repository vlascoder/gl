package MenuManagerRefactored;
use strict;
use warnings;
use base 'MenuManager';

require Carp;


sub _getMenuPosition
{
    my $self = shift;
    my $anchor = shift;

    my $position = undef;

    for (my $i = 0; $i < @{$self->{'menu'}}; $i += 2) {
        if ($self->{'menu'}->[$i] eq $anchor) {
            $position = $i;
            last;
        }
    }

    Carp::croak("Anchor menu not found") unless defined($position);

    return $position;
}

sub _getSubmenuPosition
{
    my $self = shift;
    my $menu = shift;
    my $title = shift;

    my $position = undef;

    for (my $i = 0; $i < @{$menu}; $i++) {
        if ($menu->[$i]->{'title'} eq $title) {
            $position = $i;
            last;
        }
    }

    Carp::croak("Anchor submenu not found") unless defined($position);

    return $position;
}

sub _addMenuInPosition
{
    my $self = shift;
    my ($type, $anchor, $menu_title, $menu_items) = @_;

    my $added = 0;

    my $anchor_position = $self->_getMenuPosition($anchor);

    if (defined($anchor_position)) {
        my $insert_position = $anchor_position + (($type eq 'after') ? 2 : 0);
        splice(@{$self->{'menu'}}, $insert_position, 0, $menu_title, $menu_items);

        $added++;
    }

    return $added;
}

sub _addSubmenuInPosition
{
    my $self = shift;
    my ($type, $anchor, $subanchor, @new_submenu) = @_;

    my $added = 0;

    my $anchor_position = $self->_getMenuPosition($anchor);

    if (defined($anchor_position)) {
        my $menu = $self->{'menu'}->[$anchor_position + 1];
        my $subanchor_position = $self->_getSubmenuPosition($menu, $subanchor);

        if (defined($subanchor_position)) {
            my $insert_position = $subanchor_position + (($type eq 'after') ? 1 : 0);
            splice(@{$menu}, $insert_position, 0, grep { ref($_) eq 'HASH' } @new_submenu);

            $added++;
        }
    }

    return $added;
}

1;
